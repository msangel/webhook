#!/usr/bin/env bash
# ssh root@95.213.200.133 # add host to trusted
# docker-machine create --driver generic --generic-ip-address 95.213.200.133 apphost
# docker-machine env apphost
# eval $(docker-machine env apphost)
# docker-machine regenerate-certs apphost
# docker build --no-cache -t registry.gitlab.com/msangel/webhook .
# docker push registry.gitlab.com/msangel/webhook
# docker run --detach -p 8080:8080 registry.gitlab.com/msangel/webhook:latest
docker-machine env apphost
eval $(docker-machine env apphost)
docker build --no-cache -t registry.gitlab.com/msangel/webhook .
docker push registry.gitlab.com/msangel/webhook
docker run -p 8080:8080 registry.gitlab.com/msangel/webhook:latest
