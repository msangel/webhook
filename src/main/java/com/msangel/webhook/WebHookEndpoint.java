package com.msangel.webhook;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vasyl.khrystiuk on 04/09/2019.
 */
@RestController
public class WebHookEndpoint {
    @GetMapping("hello")
    public String hello() {
        return "hello";
    }

    @PostMapping("webhook")
    public String webHook(@RequestBody JsonNode input, @RequestHeader MultiValueMap headers) {
        System.out.println("--- input ---");
        System.out.println(input);
        System.out.println("--- headers ---");
        System.out.println(headers);
        System.out.println("--- end ---");
        return "ok";
    }

    private boolean isMergeRequestTrigger(JsonNode input) {
        return true;
    }

}
